FROM python:3.9

WORKDIR /app

# Install system dependencies
RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

# Now install your Python dependencies
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

COPY . .

CMD ["gunicorn", "-b", "0.0.0.0:5000", "sca-tes4"]

